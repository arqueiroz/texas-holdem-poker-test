package andre.poker;

import java.util.List;
import java.util.Scanner;

public class Util {
    public static String getUserInputString(String question){
        Scanner scanner = new Scanner(System.in);
        System.out.println(question);
        return scanner.next();

    }

    private static Integer getUserInputInteger(String question, String exceptionMessage){
        Scanner scanner = new Scanner(System.in);
        while(true) {
            try {
                System.out.println(question);
                return scanner.nextInt();
            } catch (Exception e) {
                System.out.println(exceptionMessage);
                scanner = new Scanner(System.in);
            }
        }
    }

    public static Integer getUserInputIntegerWithingRange(String question, String exceptionMessage, String outOfRangeMessage, int min, int max){
        while (true){
            int userInput = getUserInputInteger(question,exceptionMessage);
            if(userInput >= min && userInput <= max){
                return userInput;
            }else{
                System.out.println(outOfRangeMessage);
            }
        }

    }

    private static Integer getNextPlayerIndex(List<Player> players, int currentPlayerIndex, boolean isRotationDirectionAhead){
        int increment = isRotationDirectionAhead? 1: -1;
        int nextPlayerIndex = currentPlayerIndex;
        while(true){
            nextPlayerIndex = (nextPlayerIndex + increment) == players.size() ? 0 : nextPlayerIndex + increment;

            if(players.get(nextPlayerIndex).isActiveInHand()){
                return nextPlayerIndex;
            }
        }
    }
    /**
     * Get next player to act excluding all players that had already folded
     * @param players
     * @param currentPlayerIndex
     * @return
     */
    public static Integer getNextPlayerIndex(List<Player> players, int currentPlayerIndex){
        return getNextPlayerIndex(players,currentPlayerIndex,true);
    }

    /**
     * Get previous player to act excluding all players that had already folded
     * @param players
     * @param currentPlayerIndex
     * @return
     */
    public static Integer getPreviousPlayerIndex(List<Player> players, int currentPlayerIndex){
        return getNextPlayerIndex(players,currentPlayerIndex,false);
    }
}
