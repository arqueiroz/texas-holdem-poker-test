package andre.poker;

import java.util.Arrays;
import java.util.List;


/**
 * Utility class to encapsulate all code that manages the betting rounds
 * i.e. Pre-flop, The Flop, The Turn, and The River
 * The each round demands User input
 */
public class BettingRounds {

    private static final String PRE_FLOP_ROUND = "Pre-flop";
    private static final String THE_FLOP_ROUND = "The Flop";
    private static final String THE_TURN_ROUND = "The Turn";
    private static final String THE_RIVER_ROUND = "The River";
    private static final List<String> ROUND_LIST = Arrays.asList(PRE_FLOP_ROUND, THE_FLOP_ROUND, THE_TURN_ROUND, THE_RIVER_ROUND);

    private static final int FOLD = 1;
    private static final int CHECK = 2;
    private static final int RAISE = 3;
    private static final int CALL = 4;

    private static int lastRaiserIndex;

    /**
     * Run all Rounds, from pre-flop to the river (all the rounds that enable betting)
     *
     * @param players
     * @param bigBlindIndex
     * @param remainingDeck
     * @param communityCards
     * @return
     */
    public static int run(
            List<Player> players,
            int bigBlindIndex,
            List<Card> remainingDeck,
            List<Card> communityCards
    ) {
        lastRaiserIndex = -1;
        int playerTurnIndex = Util.getNextPlayerIndex(players, bigBlindIndex);

        int nextCardIndex = 0;
        for (String round : ROUND_LIST) {
            if (round.equals(THE_FLOP_ROUND)) {
                communityCards.addAll(remainingDeck.subList(nextCardIndex, nextCardIndex + 3));
                nextCardIndex += 3;
            } else if (round.equals(THE_TURN_ROUND) || round.equals(THE_RIVER_ROUND)) {
                communityCards.add(remainingDeck.get(nextCardIndex));
                nextCardIndex++;
            }

            System.out.println("====== Round " + round + " started! ======");
            System.out.println("Community Cards: " + communityCards);
            playerTurnIndex = bettingEngine(players, playerTurnIndex);

            System.out.println(" --- END of ROUND ---");
        }

        return lastRaiserIndex;
    }

    /*
    Controls all the bets and users actions
     */
    private static Integer bettingEngine(List<Player> players, int playerTurnIndex) {

        //last player to raise the bet
        //It will be used to finish the round when it reaches the turn of the last raiser
        //It is initialized with the player next to the Big blind, if the round reach this player again, it means no player made raises, thus finish the round
        lastRaiserIndex = playerTurnIndex;
        boolean firstTurn = true;
        while (true) {
            if (!firstTurn && lastRaiserIndex == playerTurnIndex) {
                //reached the last raiser, it means end of the round
                return playerTurnIndex;
            } else if (players.stream().filter(p -> p.isActiveInHand()).count() < 2) {
                // All players folded but 1, end the Round and the Hand
                return 0;
            }

            // Print players stats
            for (Player player : players) {
                System.out.println(player.getPlayerStats());
            }

            Player actingPlayer = players.get(playerTurnIndex);

            boolean validAction;
            if (players.get(playerTurnIndex).isBot()) {
                validAction = actingPlayer.getBotBettingAction(players);
            } else {
                validAction = askHumanAction(players, playerTurnIndex, actingPlayer);
            }


            if (validAction) {
                playerTurnIndex = Util.getNextPlayerIndex(players, playerTurnIndex);
            }
            firstTurn = false;
        }
    }

    private static boolean askHumanAction(List<Player> players, int playerTurnIndex, Player actingPlayer) {
        System.out.println(actingPlayer.getName() + "'s turn:");
        int userAction = Util.getUserInputIntegerWithingRange(
                "(1) Fold (2) Check (3) Raise (4) Call",
                "Please, inform a number from 1 to 4",
                "Please, choose an option within the range",
                1,
                4);

        boolean validAction = false;

        switch (userAction) {
            case FOLD:
                validAction = actingPlayer.performFold();
                if(lastRaiserIndex == playerTurnIndex){
                    lastRaiserIndex = Util.getNextPlayerIndex(players,lastRaiserIndex);
                }
                break;
            case CHECK:
                validAction = actingPlayer.performCheck(players);
                break;
            case RAISE:
                validAction = actingPlayer.performRaise(players);
                if (validAction) {
                    lastRaiserIndex = playerTurnIndex;
                }
                break;
            case CALL:
                validAction = actingPlayer.performCall(players);
        }
        return validAction;
    }
}
