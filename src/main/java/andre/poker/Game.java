package andre.poker;

import java.util.*;

public class Game {
    private int initialMoney;
    private int smallBlind;
    private int bigBlind;


    final private List<Player> players = new ArrayList<Player>();
    final List<Card> deck = new ArrayList<Card>();
    public void run(boolean isDefaultGame) {

        System.out.println("Welcome to Texas Hold'em Tournament!");

        initDeck();

        if(!isDefaultGame){
            setupFromUserInput();
        } else{
            setupDefaultGame();
        }

        runHandsMainLoop();

        System.out.println(players.get(0) + " IS THE GREAT WINNER !!!");
    }

    private void runHandsMainLoop() {
        int handCount = 1;
        int smallBlindPlayerIndex = 0;
        int bigBlindPlayerIndex = 1;
        while (players.size() > 1){
            // Start a new Hand
            Collections.shuffle(deck);
            List<Card> communityCards = new ArrayList<Card>();
            System.out.println("----------------- Starting Hand n" + handCount + "-----------------");


            //Charge smallBLind and bigBlind
            players.get(smallBlindPlayerIndex).increaseBet(smallBlind);
            players.get(bigBlindPlayerIndex).increaseBet(bigBlind);

            int cardIndex = 0;
            //Give users the cards
            for (Player p : players) {
                p.getCurrentCards().clear();
                p.getCurrentCards().add(deck.get(cardIndex));
                p.getCurrentCards().add(deck.get(cardIndex+1));
                cardIndex +=2;
            }

            //Run all betting rounds
            int lastRaiser = BettingRounds.run(players, bigBlindPlayerIndex, deck.subList(cardIndex,deck.size()), communityCards);

            //Run Showdown
            Showdown.run(players, lastRaiser,  communityCards);

            //Update counters and player pointers to the next Hand
            handCount++;

            players.forEach(p -> p.clearHandVariables());
            smallBlindPlayerIndex = (smallBlindPlayerIndex +1) == players.size()? 0: smallBlindPlayerIndex +1;
            bigBlindPlayerIndex = (smallBlindPlayerIndex +1) == players.size()? 0: smallBlindPlayerIndex +1; //big Blind is always smallblind +1
        }
    }

    private void initDeck() {
        for (Card.Rank rank : Card.Rank.values()) {
            for (Card.Suit suit : Card.Suit.values()) {
                deck.add(new Card(suit, rank));
            }
        }
    }

    private void setupDefaultGame() {
        initialMoney = 10000;
        smallBlind = 10;
        bigBlind = 20;

        Player p1 = new Player("Player    1", initialMoney, false);
        Player p2 = new Player("Gambler Bot", initialMoney, true);
        players.addAll(Arrays.asList(p1,p2));
    }

    private void setupFromUserInput() {
        initialMoney = Util.getUserInputIntegerWithingRange(
                "What is the Initial Money for this game",
                "Please, enter a Integer number (e.g. 10000)",
                "Please, enter a number higher than 20",
                20,
                Integer.MAX_VALUE);
        smallBlind = Util.getUserInputIntegerWithingRange(
                "What is the Small Blind for this game?",
                "Please, enter a Integer number (e.g. 10)",
                "Please, enter a number between 1 and Initial Money/20",
                1,
                initialMoney/20);
        bigBlind = Util.getUserInputIntegerWithingRange(
                "What is the Big Blind for this game?",
                "Please, enter a Integer number (e.g. 20)",
                "Please, enter a number between the Small Blind and Initial Money/10",
                smallBlind,
                initialMoney/10);
        int numPlayers = Util.getUserInputIntegerWithingRange(
                "How many Players?",
                "Please, enter a Integer number (e.g. 6)",
                "Please, enter a number between 2 and 10",
                2,
                10
        );

        for(int i=0; i<numPlayers;i++){
            String playerName = Util.getUserInputString("What is Player " + (i+1) + " name?");
            boolean isBotPlayer = Util.getUserInputIntegerWithingRange(
                    "Is this player a Human? 0 - No  / 1 - Yes",
                    "Please, enter a Integer number (e.g. 0)",
                    "Please, enter a number between 0 and 1",
                    0,
                    1
            ) == 0;

            players.add(new Player(playerName, initialMoney, isBotPlayer ));
        }
    }

}