package andre.poker;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private String name;
    private boolean bot;
    private Integer money;
    private Integer currentBet = 0;

    private boolean activeInHand = true;

    private List<Card> currentCards = new ArrayList<Card>();

    public Player(String name, Integer money, boolean bot) {
        this.name = name;
        this.bot = bot;
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public boolean isBot() {
        return bot;
    }

    public void setBot(boolean bot) {
        this.bot = bot;
    }

    public Integer getCurrentBet(){
        return this.currentBet;
    }

    public List<Card> getCurrentCards() {
        return currentCards;
    }

    public boolean isActiveInHand() {
        return activeInHand;
    }

    public void setActiveInHand(boolean activeInHand) {
        this.activeInHand = activeInHand;
    }

    public void increaseBet(int bet){
        this.currentBet += bet;
        this.money -= bet;
    }

    public void clearHandVariables(){
        currentBet = 0;
        activeInHand = true;
    }

    /**
     * Show a String representation of all the player stats
     * @return
     */
    public String getPlayerStats(){
        String stats = "";
        stats += activeInHand ? ">>>>>>>> ": "folded< ";
        stats += name;
        stats += " Current bet: " + currentBet;
        stats += " Money: " + money;

        if(!isBot()){
            stats += " Hand:" + currentCards;
        }

        return stats;
    }

    public boolean getBotBettingAction(List<Player> players){
        Integer highestBet = getHighestBet(players);
        if(highestBet > this.getCurrentBet() && money>=highestBet){
            System.out.println(this.getName() + " CALLED");
            return this.performCall(players);
        }else if(highestBet.equals(this.getCurrentBet())){
            System.out.println(this.getName() + " CHECKED");
            return this.performCheck(players);
        }else{
            System.out.println(this.getName() + " FOLDED");
            return this.performFold();
        }
    }

    public boolean getBotShowdownAction(){
        return true;
    }

    public boolean performRaise(List<Player> players) {
        int highestBet = getHighestBet(players);
        int betDifference = highestBet - this.getCurrentBet();

        if (betDifference >= this.getMoney()) {
            System.out.println("You don't have enough money to Raise");
            return false;
        }

        int maxRaise = this.getMoney() - betDifference;
        int raiseInput = Util.getUserInputIntegerWithingRange(
                "Inform how much above the Highest bet you want to Raise:",
                "Invalid. Please, inform a Integer number",
                "Please, inform a number between 1 and" + maxRaise,
                1,
                maxRaise
        );

        this.increaseBet(raiseInput + betDifference);
        return true;
    }

    public boolean performCall(List<Player> players) {
        int highestBet = getHighestBet(players);

        if (highestBet <= this.getCurrentBet()) {
            System.out.println("Can't CALL because you has the highest bet");
            return false;
        } else if (this.getMoney() < 0) {
            System.out.println("Can't CALL, you don't have money left");
            return false;
        }

        int callBetAmount = highestBet - this.getCurrentBet();
        this.increaseBet(callBetAmount);

        return true;
    }

    public boolean performCheck(List<Player> players) {
        int highestBet = getHighestBet(players);

        if (highestBet > this.getCurrentBet()) {
            System.out.println("You can not CHECK: Highest bet is " + highestBet + " your current bet is " + this.getCurrentBet());

            return false;
        }
        return true;
    }

    /**
     * Get the highest bet among all players for the whole HAND
     * the bet of a player is the sum of all bet he/she did during this hand, including the blinds if any.
     *
     * @param players
     * @return
     */
    public static int getHighestBet(List<Player> players) {
        return players.stream().mapToInt(p -> p.getCurrentBet()).max().orElse(0);
    }

    public boolean performFold() {
        this.setActiveInHand(false);

        return true;

    }
}
