package andre.poker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Showdown {

    private static final int FOLD = 1;
    private static final int SHOWDOWN = 2;

    public static void run(List<Player> players, int roundStarterPlayerIndex, List<Card> communityCards) {
        List<Player> activePlayers = players.stream().filter(p -> p.isActiveInHand()).collect(Collectors.toList());

        List<Player> higherScorePlayers;

        if (activePlayers.size() == 1) {
            //if only one player left, no need to show down
            higherScorePlayers = activePlayers;
        } else {
            getUsersShowdownActions(players, roundStarterPlayerIndex, communityCards);

            higherScorePlayers = getPlayersTiedWithHighestScore(activePlayers, communityCards);
        }

        divideThePrizeAmongAllWinners(players, higherScorePlayers);

    }

    private static void divideThePrizeAmongAllWinners(List<Player> players, List<Player> higherScorePlayers) {
        //Get total prize summing all bets in this Hand
        Integer totalPrize = players.stream()
                .mapToInt(p -> p.getCurrentBet())
                .reduce(0, Integer::sum);

        System.out.println("");
        System.out.println("");
        higherScorePlayers.forEach(p -> {
            Integer dividedPrize = totalPrize / higherScorePlayers.size();
            p.setMoney(p.getMoney() + totalPrize);

            System.out.println(p.getName() + " WON THE HAND! earned " + dividedPrize);

        });
        System.out.println("");
        System.out.println("");
    }

    private static List<Player> getPlayersTiedWithHighestScore(List<Player> players, List<Card> communityCards) {
        List<Player> higherScorePlayers = new ArrayList<>();
        Long higherScore = -1L;
        for (Player player : players) {
            if (!player.isActiveInHand()) {
                continue;
            }
            List<Card> hand1WithCommunityCards = new ArrayList<>();
            hand1WithCommunityCards.addAll(player.getCurrentCards());
            hand1WithCommunityCards.addAll(communityCards);

            Long playerScore = Card.calculateHandleScore(hand1WithCommunityCards);
            if (playerScore > higherScore) {
                higherScore = playerScore;
                higherScorePlayers.clear();
                higherScorePlayers.add(player);
            } else if (higherScore.equals(playerScore)) {
                higherScorePlayers.add(player);
            }
        }
        return higherScorePlayers;
    }

    /**
     * Ask for input for each active (non-folded) players
     *
     * @param players
     * @param roundStarterPlayerIndex
     */
    private static void getUsersShowdownActions(List<Player> players, int roundStarterPlayerIndex, List<Card> communityCards) {
        int playerIndex = roundStarterPlayerIndex;
        System.out.println("Community Cards:" + communityCards);
        while (true) {
            Player actingPlayer = players.get(playerIndex);

            boolean isActionShowdown = false;
            if (!actingPlayer.isBot()) {
                System.out.println(actingPlayer.getName() + "'s turn:");
                int userAction = Util.getUserInputIntegerWithingRange(
                        "(1) Fold (2) Showdown",
                        "Please, inform a number from 1 to 2",
                        "Please, choose an option within the range",
                        1,
                        2);
                isActionShowdown = userAction == SHOWDOWN;
            }else{
                isActionShowdown = actingPlayer.getBotShowdownAction();
            }


            if (!isActionShowdown) {
                actingPlayer.performFold();
            } else {
                System.out.println(actingPlayer.getName() + "Hand is: " + actingPlayer.getCurrentCards());
            }

            playerIndex = Util.getNextPlayerIndex(players, playerIndex);
            if (roundStarterPlayerIndex == playerIndex) {
                break;
            }
        }
    }
}
