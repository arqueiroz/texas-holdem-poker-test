package andre.poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.lang.Math.max;

public class Card {
    public enum Suit {
        S,
        H,
        D,
        C
    }

    public enum Rank {
        N2, N3, N4, N5, N6, N7, N8, N9, N10, J, Q, K, A;

        @Override
        public String toString() {
            return super.toString().replace("N", "");
        }
    }

    Suit suit;
    Rank rank;

    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public Rank getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return rank.toString() + suit.toString();
    }


    private static final Long STRAIGH_FLUSH_BASE_SCORE = 900000000000L; // for comparison purpose, makes no difference between Straight Flush and Royal Straight Flush
    private static final Long FOUR_OF_A_KIND_BASE_SCORE = 800000000000L;
    private static final Long FULL_HOUSE_BASE_SCORE = 700000000000L;
    private static final Long FLUSH_BASE_SCORE = 600000000000L;
    private static final Long STRAIGHT_BASE_SCORE = 500000000000L;
    private static final Long THREE_OF_A_KIND_BASE_SCORE = 400000000000L;
    private static final Long TWO_PAIRS_BASE_SCORE = 300000000000L;
    private static final Long ONE_PAIRS_BASE_SCORE = 200000000000L;


    /**
     * calculates an Integer score which represents the Hand's Strenght
     *
     * @param hand
     * @return
     */
    public static Long calculateHandleScore(List<Card> hand) {
        List<Long> possibleScores = new ArrayList<>();
        possibleScores.add(calculateFlushOrStraightOrStraightFlushScore(hand));
        possibleScores.add(calculateScoresWithRepeatedCards(hand));

        return Collections.max(possibleScores);
    }

    /**
     * Searchs for the hights game found from the following list:
     * Four of a Kind
     * Full House
     * Trhee of a Kind
     * Two Pair
     * One Pair
     *
     * @param hand
     * @return
     */
    private static Long calculateScoresWithRepeatedCards(List<Card> hand) {
        List<Integer> ranks = new ArrayList<Integer>();

        //map hand to a sequence of numbers
        ranks = hand.stream()
                .mapToInt(card -> card.getRank().ordinal())
                .boxed()
                .collect(Collectors.toList());

        //group by rank
        Map<Integer, Long> rankCountMap = ranks.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<Integer> fourOfAKindRanks = getRepeatedRanks(rankCountMap, 4);
        List<Integer> threeOfAKindRanks = getRepeatedRanks(rankCountMap, 3);
        List<Integer> pairRanks = getRepeatedRanks(rankCountMap, 2);

        if (fourOfAKindRanks.size() > 0) {

            return getFourOfAKindScore(ranks, fourOfAKindRanks);
        } else if (threeOfAKindRanks.size() > 0 && pairRanks.size() > 0) {

            return getFullHouseScore(threeOfAKindRanks, pairRanks);
        } else if (threeOfAKindRanks.size() > 0) {

            return getThreeOfAKindScore(ranks, threeOfAKindRanks);
        } else if (pairRanks.size() > 1) {

            return getTwoPairScore(ranks, pairRanks);
        } else if (pairRanks.size() > 0) {

            return getOnePairScore(ranks, pairRanks);
        }

        return new Long(Collections.max(ranks));
    }

    private static Long getFourOfAKindScore(List<Integer> ranks, List<Integer> fourOfAKindRanks) {
        int repeatedRank = Collections.max(fourOfAKindRanks);
        int kicker = Collections.max(ranks.stream().filter(r -> r != repeatedRank).collect(Collectors.toList()));

        return FOUR_OF_A_KIND_BASE_SCORE + repeatedRank * 100 + kicker;
    }

    private static Long getFullHouseScore(List<Integer> threeOfAKindRanks, List<Integer> pairRanks) {
        int threeKindRepeatedRank = Collections.max(threeOfAKindRanks);
        int pairRepeatedRank = Collections.max(pairRanks);

        return FULL_HOUSE_BASE_SCORE + threeKindRepeatedRank * 100 + pairRepeatedRank;
    }

    private static Long getThreeOfAKindScore(List<Integer> ranks, List<Integer> threeOfAKindRanks) {
        int threeKindRepeatedRank = Collections.max(threeOfAKindRanks);
        int kicker = Collections.max(ranks.stream().filter(r -> r != threeKindRepeatedRank).collect(Collectors.toList()));

        return THREE_OF_A_KIND_BASE_SCORE + threeKindRepeatedRank * 100 + kicker;
    }

    private static Long getOnePairScore(List<Integer> ranks, List<Integer> pairRanks) {
        int pairRepeatedRank = Collections.max(pairRanks);
        int kicker = Collections.max(ranks.stream().filter(r -> r != pairRepeatedRank).collect(Collectors.toList()));

        return ONE_PAIRS_BASE_SCORE + pairRepeatedRank * 100 + kicker;
    }

    private static Long getTwoPairScore(List<Integer> ranks, List<Integer> pairRanks) {
        Collections.sort(pairRanks);
        pairRanks.sort(Collections.reverseOrder());

        int highPairRank = pairRanks.get(0);
        int lowPairRank = pairRanks.get(1);
        int kicker = Collections.max(ranks.stream().filter(r -> r != highPairRank).collect(Collectors.toList()));

        return TWO_PAIRS_BASE_SCORE + (highPairRank * 10000) + (100 * lowPairRank) + kicker;
    }

    private static ArrayList<Integer> getRepeatedRanks(Map<Integer, Long> rankCountMap, int repeatitionThreshold) {
        return rankCountMap.entrySet().stream()
                .filter(map -> map.getValue() == repeatitionThreshold)
                .mapToInt(map -> map.getKey())
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    private static Long calculateFlushOrStraightOrStraightFlushScore(List<Card> hand) {
        List<List<Integer>> ranksListBySuit = mapToListOfListOfRanksBySuit(hand);

        Long highestScore = 0L;
        List<Integer> ranksOfAnySuit = new ArrayList<>();

        List<List<Integer>> listOfListOfRanksBySuitToFindSequence = new ArrayList<>();
        //Search for straight flush - i.e. sequences in list of ranks of same suit
        for (List<Integer> ranks : ranksListBySuit) {

            List ranksToFindSequence = new ArrayList(); //clone list
            ranksToFindSequence.addAll(ranks);

            if (ranksToFindSequence.contains(Rank.A.ordinal())) {
                //to consider when "A" is the lower card of the sequence
                ranksToFindSequence.add(-1);
            }

            listOfListOfRanksBySuitToFindSequence.add(ranksToFindSequence); // to search for straight flushes
            ranksOfAnySuit.addAll(ranks); //to search for straights


        }

        highestScore = searchForStraightFlushScore(highestScore, listOfListOfRanksBySuitToFindSequence);
        highestScore = searchForStraightScore(highestScore, ranksOfAnySuit);
        highestScore = searchForFlushScore(ranksListBySuit, highestScore);

        return highestScore;
    }

    /**
     * Split the Hand list into 4 lists (one for each suit).
     * Each new list will have all Ranks of a suit
     *
     * @param hand
     * @return
     */
    private static List<List<Integer>> mapToListOfListOfRanksBySuit(List<Card> hand) {
        List<List<Integer>> ranksListBySuit = new ArrayList<>();
        for (Suit s : Suit.values()) {
            List<Integer> ranks = new ArrayList<Integer>();

            //map to a sequence of numbers separated by suit
            ranks = hand.stream()
                    .filter(card -> card.suit.equals(s))
                    .mapToInt(card -> card.getRank().ordinal())
                    .boxed()
                    .collect(Collectors.toList());


            ranksListBySuit.add(ranks);
        }
        return ranksListBySuit;
    }

    private static Long searchForStraightFlushScore(Long highestScore, List<List<Integer>> ranksListBySuit) {
        for (List<Integer> ranks : ranksListBySuit) {
            int highestRankOfSequence = hasSequenceOf5Numbers(ranks);
            if (highestRankOfSequence > 0) {
                highestScore = max(highestScore, STRAIGH_FLUSH_BASE_SCORE + highestRankOfSequence);
            }
        }
        return highestScore;
    }

    private static Long searchForStraightScore(Long highestScore, List<Integer> ranksOfAnySuit) {
        ranksOfAnySuit = ranksOfAnySuit.stream().distinct().collect(Collectors.toList());
        int highestRankOfSequence = hasSequenceOf5Numbers(ranksOfAnySuit);
        if (highestRankOfSequence > 0) {
            highestScore = max(highestScore, STRAIGHT_BASE_SCORE + highestRankOfSequence);
        }
        return highestScore;
    }

    private static Long searchForFlushScore(List<List<Integer>> ranksListBySuit, Long highestScore) {
        for (List<Integer> integerList : ranksListBySuit) {
            if (integerList.size() >= 5) {
                //base score + highest rank for tiebreak
                highestScore = max(highestScore, FLUSH_BASE_SCORE + Collections.max(integerList));
            }
        }
        return highestScore;
    }

    private static int hasSequenceOf5Numbers(List<Integer> ranks) {
        ranks = ranks.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
        for (int i = 0; i < ranks.size(); i++) {
            int firstRankOnSequence = ranks.get(i);
            int sequenceSize = 1;

            for (int j = i + 1; j < ranks.size(); j++) {
                boolean isNextNumberInSequence = ranks.get(j).equals(firstRankOnSequence - (j - i));

                if (!isNextNumberInSequence) {
                    break;
                } else if (j - i + 1 == 5) {
                    //it means we get a sequence of 5
                    return firstRankOnSequence;
                }
            }

        }
        return 0;
    }
}
