import andre.poker.Game;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();

        boolean isDefaultGame = false;
        if(args.length>0) {
            isDefaultGame = "defaultGame=1".equals(args[0]);
        }

        game.run(isDefaultGame);
    }
}
