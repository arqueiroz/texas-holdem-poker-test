package andre.poker;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static andre.poker.Card.Suit.*;
import static andre.poker.Card.Rank.*;
import static andre.poker.Card.calculateHandleScore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CardTest {
    Card AS = new Card(S, A);
    Card KS = new Card(S, K);
    Card QS = new Card(S, Q);
    Card JS = new Card(S, J);
    Card N10S = new Card(S, N10);
    Card N9S = new Card(S, N9);
    Card N8S = new Card(S, N8);
    Card N7S = new Card(S, N7);
    Card N6S = new Card(S, N6);
    Card N5S = new Card(S, N5);
    Card N4S = new Card(S, N4);
    Card N3S = new Card(S, N3);
    Card N2S = new Card(S, N2);

    Card AD = new Card(D, A);
    Card KD = new Card(D, K);
    Card QD = new Card(D, Q);
    Card JD = new Card(D, J);
    Card N10D = new Card(D, N10);
    Card N9D = new Card(D, N9);
    Card N8D = new Card(D, N8);
    Card N7D = new Card(D, N7);
    Card N6D = new Card(D, N6);
    Card N5D = new Card(D, N5);
    Card N4D = new Card(D, N4);
    Card N3D = new Card(D, N3);
    Card N2D = new Card(D, N2);

    Card AC = new Card(C, A);
    Card KC = new Card(C, K);
    Card QC = new Card(C, Q);
    Card JC = new Card(C, J);
    Card N10C = new Card(C, N10);
    Card N9C = new Card(C, N9);
    Card N8C = new Card(C, N8);
    Card N7C = new Card(C, N7);
    Card N6C = new Card(C, N6);
    Card N5C = new Card(C, N5);
    Card N4C = new Card(C, N4);
    Card N3C = new Card(C, N3);
    Card N2C = new Card(C, N2);

    Card AH = new Card(H, A);
    Card KH = new Card(H, K);
    Card QH = new Card(H, Q);
    Card JH = new Card(H, J);
    Card N10H = new Card(H, N10);
    Card N9H = new Card(H, N9);
    Card N8H = new Card(H, N8);
    Card N7H = new Card(H, N7);
    Card N6H = new Card(H, N6);
    Card N5H = new Card(H, N5);
    Card N4H = new Card(H, N4);
    Card N3H = new Card(H, N3);
    Card N2H = new Card(H, N2);

    List<List<Card>> listOfHandsOrderedDescByRank = new ArrayList();

    List<Card> royalStraightFlushHearts = Arrays.asList(AH, KH, QH, JH, N10H, N9C, N8D);
    List<Card> royalStraightFlushClubs = Arrays.asList(AC, N2S, N3D, KC, QC, JC, N10C);

    List<Card> StraightFlushClubsWith8rank = Arrays.asList(N2D, N7C, N8C, N3D, N4C, N5C, N6C);
    List<Card> StraightFlushDiamondsWith6rank = Arrays.asList(N2D, AD, N8D, N3D, N4D, N5D, N6D);
    List<Card> StraightFlushDiamondsWith4rank = Arrays.asList(N2D, AD, N5D, N3D, N4D, JC, N10C);

    List<Card> fourOfKindWith7KickerJ = Arrays.asList(N9D, N7C, N7D, N3D, N7H, JC, N7S);
    List<Card> fourOfKindWith4Kicker5 = Arrays.asList(N3D, N4C, N4D, N4S, N4H, N5C, N3S);
    List<Card> fourOfKindWith4Kicker6 = Arrays.asList(N3D, N4C, N4D, N4S, N4H, N6C, N3S);

    List<Card> fullHouseWith7and3KickerJ = Arrays.asList(N9D, N3C, N7D, N3D, N7H, JC, N7S);
    List<Card> fullHouseWith4and5Kicker3 = Arrays.asList(N3D, N4C, N4D, N4S, N5H, N5C, N3S);
    List<Card> fullHouseWith4and3KickerA = Arrays.asList(N3D, N4C, N4D, N4S, N3H, N5C, AS);

    List<Card> FlushDiamondWithArank = Arrays.asList(N6D, AD, N7D, N3D, N8D, JC, N10C);
    List<Card> FlushDiamondWithJrank = Arrays.asList(N6D, N2D, N7D, N3D, N8D, JD, N10C);
    List<Card> FlushClubsWith10rank = Arrays.asList(N6C, N5C, N7D, N3D, N8C, N9C, N10C);

    List<Card> StraightWithArank = Arrays.asList(AD, AC, KD, N3D, QD, JC, N10C);
    List<Card> StraightWithJrank = Arrays.asList(N9D, AC, N7D, N3D, N8D, JC, N10C);

    List<Card> threeOfKindWith7KickerJ = Arrays.asList(N9D, N7C, N7D, N3D, N2H, JC, N7S);
    List<Card> threeOfKindWith4Kicker7 = Arrays.asList(N3D, N7C, N4D, N4S, N4H, N5C, N2S);
    List<Card> threeOfKindWith4Kicker5 = Arrays.asList(N3D, N4C, N3D, N4S, N4H, N5C, N3S);


    List<Card> twoPairOf7and2KickerA = Arrays.asList(N2D, N7C, N7D, N3D, N2D, N3H, AC);
    List<Card> twoPairOf7and3KickerJ = Arrays.asList(N9D, N7C, N7D, N3D, N3D, JC, N10C);
    List<Card> twoPairOf6and3KickerA = Arrays.asList(N2D, N6C, N6D, N3D, N2D, N3H, AC);
    List<Card> twoPairOf6and2KickerA = Arrays.asList(N2D, N6C, N6D, N3D, N2D, KH, AC);
    List<Card> twoPairOf5and4KickerA = Arrays.asList(N4D, N5C, N5D, KD, N4D, N3H, AC);

    List<Card> OnePairOfAKickerK = Arrays.asList(KD, AC, AD, N3D, N2D, JC, N10C);
    List<Card> OnePairOfAKickerQ = Arrays.asList(QD, AC, AD, N3D, N2D, JC, N10C);
    List<Card> OnePairOf7KickerJ = Arrays.asList(N9D, N7C, N7D, N3D, N2D, JC, N10C);
    List<Card> OnePairOf6KickerJ = Arrays.asList(N9D, N6C, N6D, N3D, N2D, JC, N10C);
    List<Card> OnePairOf6Kicker10 = Arrays.asList(N9D, N6C, N6D, N3D, N2D, N8H, N10C);

    List<Card> kickerA = Arrays.asList(N9D, AC, N5D, N3C, QD, N8H, N10C);
    List<Card> kickerQ = Arrays.asList(N9D, N6C, N5D, N3C, QD, N8H, N10C);
    List<Card> kicker10 = Arrays.asList(N9D, N6C, N5D, N3C, N2D, N8H, N10C);


    @Test
    public void testListOfHandsComparison() {
        listOfHandsOrderedDescByRank = Arrays.asList(
                //Straight flushe
                royalStraightFlushHearts,
                StraightFlushClubsWith8rank,
                StraightFlushDiamondsWith6rank,
                StraightFlushDiamondsWith4rank,
                //four of a kind
                fourOfKindWith7KickerJ,
                fourOfKindWith4Kicker6,
                fourOfKindWith4Kicker5,
                //Full House
                fullHouseWith7and3KickerJ,
                fullHouseWith4and5Kicker3,
                fullHouseWith4and3KickerA,
                //Flush
                FlushDiamondWithArank,
                FlushDiamondWithJrank,
                FlushClubsWith10rank,
                //Straight
                StraightWithArank,
                StraightWithJrank,
                //Three of a kind
                threeOfKindWith7KickerJ,
                threeOfKindWith4Kicker7,
                threeOfKindWith4Kicker5,
                //TwoPair
                twoPairOf7and2KickerA,
                twoPairOf7and3KickerJ,
                twoPairOf6and3KickerA,
                twoPairOf6and2KickerA,
                twoPairOf5and4KickerA,
                //OnePair
                OnePairOfAKickerK,
                OnePairOfAKickerQ,
                OnePairOf7KickerJ,
                OnePairOf6KickerJ,
                OnePairOf6Kicker10,
                //Kicker
                kickerA,
                kickerQ,
                kicker10
        );


        for (int i = 0; i < listOfHandsOrderedDescByRank.size() - 1; i++) {
            Long h1 = calculateHandleScore(listOfHandsOrderedDescByRank.get(i));
            Long h2 = calculateHandleScore(listOfHandsOrderedDescByRank.get(i + 1));
            assertTrue("List of Hands should be in descending order," +
                            " failed with index " + i + " and " + (i + 1) +
                            " got values " + h1 + " and" + h2,
                    h1 > h2);
        }
    }

    private void compareEqualHands(List<Card> hand1, List<Card> hand2) {
        Long h1 = calculateHandleScore(hand1);
        Long h2 = calculateHandleScore(hand2);
        assertEquals("Hands should be equal: " + hand1 + " " + hand2, h1, h2);
    }

    @Test
    public void testRoyalStraightFlushEqualComparison() {
        compareEqualHands(royalStraightFlushHearts, royalStraightFlushClubs);
    }


    @Test
    public void testFourOfAKindEqualComparison() {
        compareEqualHands(
                Arrays.asList(AD, AC, AS, AH, QD, N8H, N10C),
                Arrays.asList(AD, AC, AS, QC, QD, QH, AH)
        );
    }

    @Test
    public void testFullHouseEqualComparison() {
        compareEqualHands(
                Arrays.asList(KD, N3C, N7D, N3D, N7H, JC, N7S),
                Arrays.asList(N9D, N7C, N7D, N3D, N3H, AC, N7S)
        );
    }

    @Test
    public void testTwoPairEqualComparison() {
        compareEqualHands(
                Arrays.asList(N2D, N7C, N7D, N3D, N2D, N3H, AC),
                Arrays.asList(KD, N7C, N7D, N3D, QD, N3H, AC)
        );
    }

    @Test
    public void testKickerEqualComparison() {
        compareEqualHands(
                Arrays.asList(N9D, AC, N5D, N3C, QD, N8H, N10C),
                Arrays.asList(N9D, AC, KD, N3C, QD, JH, N8C)
        );
    }

}
